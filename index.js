$(document).ready(function(){
   $("#alerta").css('display','none')
   $("#txtIdentificacion").numeric();
   $("#txtTelefono").numeric();
 
   traerUsusarios();
   $("#txtIdentificacion").change(function(){
       if($(this).val().length>3 && $(this).val().length<12){
        $("#alerta").text('')
        $("#alerta").css('display','none');
            $.ajax({
                url:'https://gorest.co.in/public/v2/users/'+$(this).val()
        }).then(function(data){
                $("#txtNombre").val(data.name)
                $("#txtGenero").val(data.gender)
                $("#txtCorreo").val(data.email)
        }).fail(function(){
            $("#txtNombre").val('')
            $("#txtGenero").val('')
            $("#txtCorreo").val('')
            mostrarMensaje("Usuario con identificacion "+$("#txtIdentificacion").val()+" no existe" );
        })
       }else{
        mostrarMensaje("Documento debe contener entre 4 y 11 caracteres");
       }       
   });

   $("#btnGuardar").click(function(){
         $("#alerta").css('display','none');       
         const formato = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
         if (formato.test($("#txtNombre").val())) {           
            mostrarMensaje('No puedes ingresar caracteres especiales en el campo nombre');
            return false;
         } 
         if ($("#txtNombre").val().length>100) {           
            mostrarMensaje('No se permiten mas de 100 caracteres en el campo nombre');
            return false;
         } 
         if ($("#txtTelefono").val().length!==10) {           
            mostrarMensaje('El campo telefono debe tener 10 caracteres');
            return false;
         } 
         if($("#txtTelefono").val().charAt(0)!=='3' && $("#txtTelefono").val().charAt(0)!=='6'){            
            mostrarMensaje('El campo telefono debe tener iniciar por 3 o por 6');
            return false;
         }
         var regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
         if (!regex.test($("#txtCorreo").val())){            
            mostrarMensaje("La dirección de email es incorrecta.");
            return false;
        }
        $.ajax({
            url:'Guardar.php',
            data : {
                'identification':$("#txtIdentificacion").val(),
                'nombre':$("#txtNombre").val(),
                'genero':$("#txtGenero").val(),
                'telefono':$("#txtTelefono").val(),
                'correo' : $("#txtCorreo").val()
            },
            success: function(response){
                mostrarMensaje(response);
                traerUsusarios();
            }
        })   
   });
});

function mostrarMensaje(mensaje){
    $("#alerta").css('display','block');
        $("#alerta").text(mensaje);
}

function traerUsusarios(){
    $.ajax({
        url:'Consultar.php',
        success: function(response){
          $("#tblBody").html(response);
        }
        })
}