<html>
    <head>
        <title>Prueba Entelco</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery.numeric@1.0.0/jquery.numeric.min.js" ></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="index.js" ></script>
    </head>
    <body>
        <form id='formulario'>
            <table>
                <tr>
                    <td colspan='2'><h1>Formulario de creacion</h1></td>
                </tr>
                <tr>
                    <td>Identificación</td>
                    <td><input id='txtIdentificacion' class='form-control input-sm' name='identification' ></td>
                </tr>
                <tr>
                    <td>Nombre</td>
                    <td><input id='txtNombre' class='form-control input-sm' name='nombre' ></td>
                </tr>
                <tr>
                    <td>Género</td>
                    <td><input id='txtGenero' class='form-control input-sm' name='genero' ></td>
                </tr>
                <tr>
                    <td>Número de télefono</td>
                    <td><input id='txtTelefono'  class='form-control input-sm' name='telefono' ></td>
                </tr>
                <tr>
                    <td>Correo eléctronico</td>
                    <td><input id='txtCorreo' type='email' class='form-control input-sm' name='correo' ></td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <div id='alerta' class="alert alert-info" role="alert">                           
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'><button type='button' id='btnGuardar' class='btn btn-primary'>Crear</button></td>
                </tr>
            </table>
        </form>

        <hr/>
        <h1>Usuarios creados</h1>
        <table class='table table-striped'>
        <tr>
            <td>Identificación</td>            
            <td>Nombre</td>            
            <td>Género</td>       
            <td>Número de télefono</td>            
            <td>Correo eléctronico</td>
         </tr>
         <tbody id='tblBody'></tbody>
        </table>
    </body>
</html>